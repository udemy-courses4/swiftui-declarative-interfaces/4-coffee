//
//  CoffeeApp.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import SwiftUI

@main
struct CoffeeApp: App {
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
