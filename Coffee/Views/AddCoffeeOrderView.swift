//
//  AddCoffeeOrderView.swift
//  Coffee
//
//  Created by Sander de Groot on 19/02/2021.
//

import SwiftUI

struct AddCoffeeOrderView: View {
    
    @Binding var isPresented: Bool
    @ObservedObject private var addCoffeeOrderVM = AddCoffeeOrderViewModel()
    
    var body: some View {
        NavigationView {
            VStack {
                Form {
                    Section(header: Text("INFORMATION").font(.body)) {
                        TextField("Enter name", text: $addCoffeeOrderVM.name)
                    }
                    Section(header: Text("SELECT COFFEE").font(.body)) {
                        ForEach(addCoffeeOrderVM.coffeeList, id: \.name) { coffee in
                            CoffeeCellView(coffee: coffee, selection: $addCoffeeOrderVM.coffeeName)
                        }
                    }
                    Section(
                        header: Text("SELECT SIZE").font(.body),
                        footer: OrderTotalView(total: addCoffeeOrderVM.total)
                    ) {
                        Picker("", selection: $addCoffeeOrderVM.size) {
                            Text("Small").tag("Small")
                            Text("Medium").tag("Medium")
                            Text("Large").tag("Large")
                        }
                        .pickerStyle(SegmentedPickerStyle())
                    }
                }
                
                HStack {
                    Button("Place Order") {
                        addCoffeeOrderVM.placeOrder()
                        isPresented = false
                    }
                    .padding(EdgeInsets(top: 12, leading: 100, bottom: 12, trailing: 100))
                    .foregroundColor(.white)
                    .background(Color(red: 46/255, green: 204/255, blue: 113/255))
                    .cornerRadius(10)
                }
                
            }
            .navigationBarTitle("Add Order")
        }
    }
}

struct CoffeeCellView: View {
    
    let coffee: CoffeeViewModel
    @Binding var selection: String
    
    var body: some View {
        return HStack {
            Image(coffee.imageURL)
                .resizable()
                .frame(width: 85, height: 85)
                .cornerRadius(16)
            Text(coffee.name)
                .font(.title)
                .padding([.leading], 10)
            Image(systemName: selection == coffee.name ? "checkmark" : "")
                .padding()
        }
        .onTapGesture {
            selection = coffee.name
        }
    }
}

struct AddCoffeeOrderView_Previews: PreviewProvider {
    
    static var previews: some View {
        AddCoffeeOrderView(isPresented: .constant(false))
    }
}
