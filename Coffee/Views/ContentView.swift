//
//  ContentView.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject private var orderListVM = OrderListViewModel()
    @State private var showModal = false
    
    var body: some View {
        NavigationView {
            OrderListView(orders: orderListVM.orders)
                .navigationBarTitle("Coffee Orders")
                .navigationBarItems(
                    leading: Button(action: reloadOrders, label: {
                        Image(systemName: "arrow.clockwise")
                            .foregroundColor(.black)
                    }),
                    trailing:Button(action: showAddCoffeeOrderView, label: {
                        Image(systemName: "plus")
                            .foregroundColor(.black)
                    })
                )
                .sheet(isPresented: $showModal, content: {
                    AddCoffeeOrderView(isPresented: $showModal)
                })
        }
        .onAppear {
            orderListVM.fetchOrders()
        }
    }
    
    private func reloadOrders() {
        orderListVM.fetchOrders()
    }
    
    private func showAddCoffeeOrderView() {
        showModal = true
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewDevice("iPhone 12 Pro")
    }
}
