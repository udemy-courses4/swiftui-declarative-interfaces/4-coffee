//
//  OrderListView.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import SwiftUI

struct OrderListView: View {
    
    private let orders: [OrderViewModel]
    
    init(orders: [OrderViewModel]) {
        self.orders = orders
    }
    
    var body: some View {
        List {
            ForEach(self.orders, id: \.id) { order in
                
                HStack(spacing: 10) {
                    Image(order.coffeeName)
                        .resizable()
                        .frame(width: 100, height: 100)
                        .cornerRadius(16)
                    
                    VStack(alignment: .leading) {
                        Text(order.name)
                            .font(.title)
                            .padding([.bottom], 5)
                        
                        HStack {
                            Text(order.coffeeName)
                                .foregroundColor(.white)
                                .padding(6)
                                .background(Color.green)
                                .cornerRadius(10)
                            
                            Text(order.size)
                                .foregroundColor(.white)
                                .padding(6)
                                .background(Color.blue)
                                .cornerRadius(10)
                        }
                    }
                }
            }
        }
    }
}

struct OrderListView_Previews: PreviewProvider {
    static var previews: some View {
        let orders = [
            OrderViewModel(order: Order(name: "Mary", size: "Medium", coffeeName: "Regular", total: 2.45))
        ]
        OrderListView(orders: orders)
            .previewDevice("iPhone 12 Pro")
    }
}
