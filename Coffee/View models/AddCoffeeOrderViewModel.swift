//
//  AddCoffeeOrderViewModel.swift
//  Coffee
//
//  Created by Sander de Groot on 24/12/2020.
//

import Foundation

final class AddCoffeeOrderViewModel: ObservableObject {
    
    var name =  ""
    @Published var size = "Medium"
    @Published var coffeeName = ""
    
    lazy var coffeeList: [CoffeeViewModel] = {
        return Coffee.all().map(CoffeeViewModel.init)
    }()
    
    var total: Double {
        return calculateTotalPrice()
    }
    
    let webservice = Webservice()
    
    func placeOrder() {
        let order = Order(name: name, size: size, coffeeName: coffeeName, total: total)
        
        webservice.createOrder(order) { response in
            
        }
    }
    
    private func priceForSize() -> Double {
        let prices = ["Small": 1.0, "Medium": 2.0, "Large": 3.0]
        return prices[size]!
    }
    
    private func calculateTotalPrice() -> Double {
        let coffeeVM = coffeeList.first { $0.name == coffeeName }
        if let coffeeVM = coffeeVM {
            return coffeeVM.price * priceForSize()
        }
        return 0
    }
}
