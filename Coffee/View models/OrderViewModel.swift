//
//  OrderViewModel.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import Foundation

final class OrderViewModel {
    
    let id = UUID()
    
    var name: String {
        return order.name
    }
    
    var coffeeName: String {
        return order.coffeeName
    }
    
    var size: String {
        return order.size
    }
    
    var total: Double {
        return order.total
    }
    
    private let order: Order
    
    init(order: Order) {
        self.order = order
    }
}
