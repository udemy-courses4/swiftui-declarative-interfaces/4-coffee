//
//  OrderListViewModel.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import Foundation

final class OrderListViewModel: ObservableObject {
    
    @Published var orders = [OrderViewModel]()
    
    func fetchOrders() {
        
        Webservice().getAllOrders { (orders) in
            if let orders = orders {
                self.orders = orders.map(OrderViewModel.init(order:))
            }
        }
    }
}
