//
//  OrderListViewModel.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import Foundation

final class CoffeeListViewModel {
    
    var coffeeList: [CoffeeViewModel] = [CoffeeViewModel]()
}

struct CoffeeViewModel {
    
    var name: String {
        return self.coffee.name
    }
    
    var imageURL: String {
        return self.coffee.imageURL
    }
    
    var price: Double {
        return self.coffee.price
    }
    
    private var coffee: Coffee
    
    init(coffee: Coffee) {
        self.coffee = coffee
    }
}
