//
//  CreateOrderResponse.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import Foundation

struct CreateOrderResponse: Decodable {
    
    let success: Bool
}
