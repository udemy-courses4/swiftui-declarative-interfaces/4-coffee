//
//  Order.swift
//  Coffee
//
//  Created by Sander de Groot on 27/11/2020.
//

import Foundation

struct Order: Codable {
    
    let name: String
    let size: String
    let coffeeName: String
    let total: Double
}
